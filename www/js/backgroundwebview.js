module.exports = (function(){
	function getContent(callback) {
        cordova.exec(callback, function(err) {
            callback("It didn't work :(");
        }, "BGWebview", "getContent", []);
    }
	return {
		echo: function(str, callback) {
	        cordova.exec(callback, function(err) {
	            callback('Nothing to echo.');
	        }, "BGWebview", "echo", [str]);
	    },loadPage: function(url, callback) {
	        cordova.exec(callback, function(err) {
	            callback("It didn't work :(");
	        }, "BGWebview", "loadPage", [url]);
	    },getPage: function(pUrl,pCallback,pError){
	    	this.loadPage(pUrl,function(){
	    		checkContent();
	    	});

	    	function checkContent(){
	    		getContent(function(pHTML){
		        	vDocument = $(pHTML);
		        	if($("form#challenge-form",vDocument).length > 0){
		        		setTimeout(function(){
		        			checkContent();
		        		},1000);
		        	}else{
		        		pCallback(pHTML);
		        	}
		        });
	    	}
	    }
	}
})();