//
//  CDVBGWebview.m
//  Voatr
//
//  Created by Kyle Vogt on 8/5/15.
//
//

#import "CDVBGWebview.h"

@implementation CDVBGWebviewVC

-(id)init
{
    self = [super init];
    if(self){
        [self initHelper];
    }
    return self;
}

-(void)initHelper
{
    self.webview = [[UIWebView alloc] init];
}

-(void)load:(NSString*)url
{
    [self.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

-(CDVPluginResult*)getContent
{
    CDVPluginResult* result = nil;
    if (_webview.isLoading) {
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"webview is loading"];
    } else {
        NSString *html = [_webview stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
        result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:html];
    }
    return result;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return true;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
}

@end

@implementation CDVBGWebview

- (void)echo:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];
    
    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)loadPage:(CDVInvokedUrlCommand*)command
{
    //[self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;
        NSString* url = [command.arguments objectAtIndex:0];
        
        if (url != nil && [url length] > 0) {
            if (self.webview == nil) {
                self.webview = [[CDVBGWebviewVC alloc] init];
            }
            [_webview load:url];
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"loading page"];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    //}];
}

- (void)getContent:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    
    pluginResult = [self.webview getContent];
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
