//
//  CDVBGWebview.h
//  Voatr
//
//  Created by Kyle Vogt on 8/5/15.
//
//

#import <Cordova/CDV.h>


@interface CDVBGWebviewVC : UIViewController <UIWebViewDelegate>

@property (nonatomic,retain) UIWebView *webview;
@property (nonatomic,retain) NSString *curCallbackId;

- (void) load:(NSString*)url;
- (CDVPluginResult*) getContent;

@end

@interface CDVBGWebview : CDVPlugin
    - (void)echo:(CDVInvokedUrlCommand*)command;
    - (void)loadPage:(CDVInvokedUrlCommand*)command;
    - (void)getContent:(CDVInvokedUrlCommand*)command;

    @property (nonatomic, retain) CDVBGWebviewVC *webview;
@end